package printForBrands;
import java.awt.Robot;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.concurrent.TimeUnit;

import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class shipping {
	int i = 0;
	int j = 0;
	WebDriver driver;
	By allListMain = By.xpath("/html/body/header/div[3]/nav/div/ul");
	JavascriptExecutor jse ;
	int quantity1;
	
	
	@BeforeTest
	public void invokeBrowser()
	{
		
		System.setProperty("webdriver.chrome.driver",
				"C:\\Users\\jomin.j\\Downloads\\chromedriver_win32\\chromedriver.exe");
		driver = new ChromeDriver();

		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.manage().timeouts().pageLoadTimeout(40, TimeUnit.SECONDS);
		driver.get("http://printforbrands.webreinvent.com/index.php");
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		
		
		
	}
	
	
	
	
	
	@Test
	public void test() throws Exception {
		WebElement ele = driver.findElement(allListMain);
		List<WebElement> lstInsideAllListMain = ele.findElements(By.xpath("li/a"));
		for (WebElement ele2 : lstInsideAllListMain) {
			++i;
		
			ele2.click();
			List<WebElement> lstInsideSubList = ele.findElements(By.xpath("li/div/ul"));
			for (WebElement ele3 : lstInsideSubList) {
				
			
				List<WebElement> lstInsideSub2List = ele3.findElements(By.xpath("ul/li"));
				for (WebElement ele4 : lstInsideSub2List)  // inside the SubList2
				{
					System.out.println(ele4.getText());
				String link = 	ele4.findElement(By.xpath("a")).getAttribute("href");
					Robot robot = new Robot();
					robot.mousePress(InputEvent.BUTTON1_MASK);
					robot.mouseRelease(InputEvent.BUTTON1_MASK);
					robot.keyPress(KeyEvent.VK_CONTROL);
					robot.keyPress(KeyEvent.VK_T);
					robot.keyRelease(KeyEvent.VK_T);
					robot.keyRelease(KeyEvent.VK_CONTROL);
					ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
					++j;
					driver.switchTo().window(tabs.get(j));
					driver.navigate().to(link);
					
					WebElement inLink = driver.findElement(By.xpath("/html/body/div[2]/div/div/div[2]/div/div[2]/div[1]"));
					List<WebElement> lstInLink = inLink.findElements(By.xpath("ul"));
					
					for ( WebElement eleTest :  lstInLink) // selecting individual Products
					{
						eleTest.findElement(By.xpath("li/div/a")).click();
						jse = (JavascriptExecutor) driver;
						jse.executeScript("scroll(0,800)");
						Select sets = new Select(driver.findElement(By.xpath("//*[@id=\"sets\"]")));
						sets.selectByIndex(4);
						List<WebElement> lstDDN = driver.findElements(By.cssSelector("select[class*='required-entry product-custom-option']"));
						int checked =0;
						for(WebElement eleDDN : lstDDN)
						{
							++checked;
							Select selec = new Select(eleDDN);
							selec.selectByIndex(3);
							if(checked ==1) {
							String quant = selec.getFirstSelectedOption().getText();
							System.out.println("Check" +quant);
							quant = quant.substring(0, 3);
							int quantit = Integer.parseInt(quant);
						 quantity1 = 4*quantit;
						 System.out.println("uant  ia" +quantity1);}
							
						}
						List<WebElement> lstSubDDN = driver.findElements(By.cssSelector("select[class*='product-custom-option required-entry']"));
						for(WebElement eleSubDDN : lstSubDDN)
						{
							Select selecSubDDN = new Select(eleSubDDN);
							selecSubDDN.selectByIndex(1);
							
						}
						driver.findElement(By.xpath("//*[@id=\"product-addtocart-button\"]")).click(); // ADD to cart button
						Thread.sleep(3000);
						checkoutCart(quantity1); // Cart Function is called
					}
					
					driver.switchTo().window(tabs.get(0));
					
					

				}

			}

		}
	}
	
	public void checkoutCart(int quantity) throws Exception
	{
		driver.findElement(By.xpath("/html/body/header/div[2]/div/div/div/ul/li[5]/a")).click(); // Add to cart button
		try
		{
			
			driver.findElement(By.xpath("/html/body/div[2]/div/div/div[1]/button")).click(); // Proceed to check out button
			driver.findElement(By.xpath("//*[@id=\"login:guest\"]")).click(); // Radio button for check out as guest
			driver.findElement(By.xpath("//*[@id=\"onepage-guest-register-button\"]/span/span")).click();
			driver.findElement(By.xpath("//*[@id=\"billing:firstname\"]")).sendKeys("tst");
			driver.findElement(By.xpath("//*[@id=\"billing:lastname\"]")).sendKeys("tst");
			driver.findElement(By.xpath("//*[@id=\"billing:email\"]")).sendKeys("tst@gmail.in");
			driver.findElement(By.xpath("//*[@id=\"billing:street1\"]")).sendKeys("tst");
			driver.findElement(By.xpath("//*[@id=\"billing:city\"]")).sendKeys("tst");
			driver.findElement(By.xpath("//*[@id=\"billing:postcode\"]")).sendKeys("92008");
			
			
			
			Select selecCountry = new Select(driver.findElement(By.xpath("//*[@id=\"billing:country_id\"]")));
			selecCountry.selectByValue("US");
			Select selecState = new Select(driver.findElement(By.xpath("//*[@id=\"billing:region_id\"]")));
			selecState.selectByIndex(2);
			
			
			driver.findElement(By.xpath("//*[@id=\"billing-buttons-container\"]/div/button/span/span")).click();
			
			
			
			
		}	catch(StaleElementReferenceException e) {System.out.println("");}
			catch (NoSuchElementException e ) {System.out.println("");}
		Thread.sleep(3000);
		Select shippingMethods = new Select(driver.findElement(By.xpath("//*[@id=\"shipping_method\"]")));
		List<WebElement> lstShipping = 	shippingMethods.getOptions();
		int jk =0;
		
		for(WebElement lstShippingMethods : lstShipping)
		{
			++jk;
			try
			{
			String method = lstShippingMethods.getText();
			String numOnly = method.replaceAll("[^0123456789.]", "" );
			numOnly = numOnly.substring(2, 5);
			
				
			float ActualValue = Float.parseFloat(numOnly);
			float ExpectedValue;
			
			if(jk ==1)
			{
				File src  = new File("C:\\Users\\jomin.j\\Documents\\ExpressUSMails.xlsx");
				FileInputStream fis = new FileInputStream(src);
				XSSFWorkbook wb = new XSSFWorkbook(fis);
				XSSFSheet sheet   =	wb.getSheetAt(0); // row 2 , cell 5
				XSSFCell cell;
				String[] rangeMin =  new String[5];
				String[] rangeMax =  new String[5];
				float[] rangeMinInt =  new float[5];
				float[] rangeMaxInt =  new float[5];
				float[] PriceInt =  new float[5];
				String[] price =  new String[5];
				int k= 0;
				System.out.println("Outside");
				 DataFormatter formatter = new DataFormatter();
				for(int x = 0 ; x<5 ; x++) {
					System.out.println("Inside");
					
				cell = sheet.getRow(k+1).getCell(5);
				 rangeMin[k] = formatter.formatCellValue(cell);
				 rangeMinInt[k] = Float.parseFloat(rangeMin[k]);
				 
				cell = sheet.getRow(k+1).getCell(6);
				 rangeMax[k] = formatter.formatCellValue(cell);
				 rangeMaxInt[k] = Float.parseFloat(rangeMax[k]);
				 
				 cell = sheet.getRow(k+1).getCell(7);
				 price[k] = formatter.formatCellValue(cell);
				 PriceInt[k] = Float.parseFloat(price[k]);
				 
				 
				
				 
				 ++k;
				 
				 
				
				 
				}
				
			fis.close();
			for(int jus = 0; jus<5 ;++jus)
			{
				System.out.println("Finally");
			 if(quantity>rangeMinInt[jus] && quantity <rangeMaxInt[jus] )
			 {
				 ExpectedValue  = PriceInt[jus];
				 if(ActualValue == ExpectedValue)
				 {
					 System.out.println("True");
				 }
				 else
				 {
					 System.out.println("False");
					 System.err.println("Expected -- "+ExpectedValue);
					 System.err.println("Actual -- "+ActualValue);
					 
				 }
				 
				 
				 
			 }
			}
			 
			 
			}
			
			if(j==3) {
				File src  = new File("C:\\Users\\jomin.j\\Documents\\PriorityUSMails.xlsx");
				FileInputStream fis = new FileInputStream(src);
				XSSFWorkbook wb = new XSSFWorkbook(fis);
				XSSFSheet sheet   =	wb.getSheetAt(0); // row 2 , cell 5
				XSSFCell cell;
				String[] rangeMin =  new String[5];
				String[] rangeMax =  new String[5];
				String[] price =  new String[5];
				int k= 0;
				System.out.println("Outside");
				 DataFormatter formatter = new DataFormatter();
				for(int x = 0 ; x<5 ; x++) {
					System.out.println("Inside");
					
				cell = sheet.getRow(k+1).getCell(5);
				 rangeMin[k] = formatter.formatCellValue(cell);
				
				cell = sheet.getRow(k+1).getCell(6);
				 rangeMax[k] = formatter.formatCellValue(cell);
				 
				 cell = sheet.getRow(k+1).getCell(7);
				 price[k] = formatter.formatCellValue(cell);
				 
				 
				 ++k;
				}
				
			fis.close();
				
			}
			
			else
			{
				
				System.out.println("Standard");
				
			}
				
			}catch(NumberFormatException e ) {System.out.println("Empty");}
			catch(StringIndexOutOfBoundsException e ) {System.out.println("Free Shipping");}
			
		}
		
	
		
		
		
		
		
		
		
		
	}
	
	@AfterTest
	public void tearDown()
	{
		driver.close();
		
		
	}
	
	

}
